package model;

public class Moderador extends Usuario {

	private String grupo;

	public Moderador() {
		super();
	}

	public Moderador(String nome, String estado, String cidade, int cpf,
			String senha) {
		super(nome, estado, cidade, cpf, senha);
	}

	public Moderador(String grupo) {
		super();
		this.grupo = grupo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

}
