package model;
public class Reivindicacoes {

	private String tema;
	private String texto;

	public Reivindicacoes(String tema, String texto) {
		super();
		this.tema = tema;
		this.texto = texto;
	}

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}
