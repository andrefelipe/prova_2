package model;
public class Usuario {

	private String nome;
	private String estado;
	private String cidade;
	private int cpf;
	private String senha;

	public Usuario() {
		super();
	}

	public Usuario(String nome, String estado, String cidade, int cpf,
			String senha) {
		super();
		this.nome = nome;
		this.estado = estado;
		this.cidade = cidade;
		this.cpf = cpf;
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public int getCpf() {
		return cpf;
	}

	public void setCpf(int cpf) {
		this.cpf = cpf;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
